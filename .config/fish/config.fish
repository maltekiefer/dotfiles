if status is-interactive
    # Commands to run in interactive sessions can go here
end


alias cat="bat"

alias tb="nc termbin.com 9999"
alias ls='lsd -al --color=always --group-directories-first' # my preferred listing
alias la='lsd -a --color=always --group-directories-first'  # all files and dirs
alias ll='lsd -l --color=always --group-directories-first'  # long format
alias lt='lsd -aT --color=always --group-directories-first' # tree listing
alias l.='lsd -a | egrep "^\."'

alias cp="cp -iv"
alias mv="mv -iv"
alias rm="rm -iv"
alias rmf="rm -ivf"


starship init fish | source

