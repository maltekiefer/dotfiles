# Dotfiles

## Install
### Vim
```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
```

### Config
```bash
ln -sf /Users/username/Entwicklung/dotfiles/.vimrc /Users/username/
ln -sf /Users/username/Entwicklung/dotfiles/.vmrc /Users/username/
ln -sf /Users/username/Entwicklung/dotfiles/.gitignore_global /Users/username/
ln -sf /Users/username/Entwicklung/dotfiles/.gitconfig /Users/username/
ln -sf /Users/username/Entwicklung/dotfiles/.config/fish/config.fish /Users/username/.config/fish/
ln -sf /Users/username/Entwicklung/dotfiles/.config/starship.toml /Users/username/.config/
```

### brew
```bash
brew tap homebrew/cask-fonts
cat brew.packages | xargs brew install
cat brew-cask.packages | xargs brew install
brew search '/font-.*-nerd-font/' | awk '{ print $1 }' | xargs -I{} brew install --cask {} || true
```
